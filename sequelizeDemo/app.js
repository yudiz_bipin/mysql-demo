const express = require("express");
const app = express();

require("./model/index");
const userController = require("./controller/userController");

app.use(express.json());

app.get("/", (req, res) => {
  res.send("homepage");
});

app.get("/add", userController.addUser);

app.get("/crud", userController.crud);

app.get("/query", userController.queryData);

app.get("/onetoone", userController.oneToOne);

app.get("/belongsto", userController.belongsTo);

app.get("/hasmany", userController.hasMany);

app.get("/manytomany", userController.manyToMany);

app.get("/transaction", userController.transaction);

app.get("/managed", userController.managedTransaction);

app.listen(8000, () => console.log("server is running on port :: 8000"));
