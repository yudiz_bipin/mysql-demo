const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize("demo", "root", "bipin123", {
  host: "localhost",
  dialect: "mysql",
  logging: false,
  pool: { max: 5, min: 0, idle: 10000 },
  //! global hook
  // define: {
  //   hooks: {
  //     beforeValidate() {
  //       console.log("global hook called");
  //     },
  //   },
  // },
});

sequelize
  .authenticate()
  .then(() => {
    console.log("connected");
  })
  .catch((err) => {
    console.log("error", err);
  });

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

// (async function abc() {
//   console.log("transaction", await sequelize.transaction());
// })();

db.sequelize.sync({ force: false, engine: "innoDB" }).then(() => {
  console.log("re-synced");
});

//importing models
db.users = require("../model/users.js")(sequelize, DataTypes);
db.post = require("../model/post")(sequelize, DataTypes);
db.tags = require("../model/tags")(sequelize, DataTypes);
db.post_tag = require("../model/post_tag")(sequelize, DataTypes);

//associations

//! one to one
db.users.hasOne(db.post, { foreignKey: "user_id" });

//! one to many
db.users.hasMany(db.post, { foreignKey: "user_id", as: "postDetails" });
db.post.belongsTo(db.users, { foreignKey: "user_id", as: "author" });

//! many to many

db.post.belongsToMany(db.tags, { through: "post_tag" });
db.tags.belongsToMany(db.post, { through: "post_tag" });

module.exports = db;
