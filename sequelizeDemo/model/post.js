module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define("post", {
    post_name: DataTypes.STRING,
    user_id: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: true,
      },
    },
  });

  return Post;
};
