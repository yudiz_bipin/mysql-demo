module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    "users",
    {
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        defaultValue: "test@gmail.com",
        validate: {
          isEmail: true,
        },
      },
      gender: {
        type: DataTypes.STRING,
      },
    },
    {
      //   tableName: "user-data",
      timestamps: false,

      //! local hook
      // hooks: {
      //   beforeValidate() {
      //     console.log("hook called before validation");
      //   },
      //   afterValidate() {
      //     console.log("hook called after validation");
      //   },
      // },
    }
  );

  //!permanent hook
  // Users.addHook("beforeValidate", (user, options) => {
  //   console.log("add hook called");
  // });
  return Users;
};
