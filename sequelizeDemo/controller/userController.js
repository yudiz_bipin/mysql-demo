const { sequelize } = require("../model/index");
const db = require("../model/index");
const post = require("../model/post");
const Users = db.users;
const Post = db.post;
const Tags = db.tags;
const Post_Tag = db.post_tag;

const addUser = async (req, res) => {
  //   const data = await Users.build({
  //     name: "bipin",
  //     email: "bipin@gmail.com",
  //   });

  //   await data.save();

  const data = await Users.create({
    name: "hulk3",
    email: "hulk3@gmail.com",
    gender: "male",
  });

  console.log(data.dataValues);

  //update
  //   data.name = "evilshooter";

  //delete
  //   data.destroy();

  // data.save();
  res.json(data);
};

const crud = async (req, res) => {
  //! insert user

  //   const data = await Users.create({
  //     name: "arron",
  //     email: "bipin@gmail.com",
  //     gender: "male",
  //   });

  //! update record

  // const data = await Users.update(
  //   { name: "arronstone" },
  //   { where: { id: 11 } }
  // );

  //! find a record

  // const data = await Users.findOne({ where: { id: 11 } });
  // console.log(data.dataValues);

  //! delete record

  // const data = await Users.destroy({ where: { id: 11 } });

  //! destory all

  // const data = await Users.destroy({ truncate: true });

  //! bulkCreate

  const data = await Users.bulkCreate([
    { name: "arron", email: "arron@gmail.com", gender: "male" },
    {
      name: "bipin",
      email: "bipin@gmail.com",
      gender: "male",
    },
    {
      name: "anuj",
      email: "anuj@gmail.com",
      gender: "male",
    },
    {
      name: "shrey",
      email: "shrey@gmail.com",
      gender: "male",
    },
    {
      name: "ravi",
      email: "ravi@gmail.com",
      gender: "male",
    },
    {
      name: "black widow",
      email: "blackwidow@gmail.com",
      gender: "female",
    },
  ]);

  res.send("changes saved");
};

const queryData = (req, res) => {
  res.send("query data");
};

const oneToOne = async (req, res) => {
  const data = await Users.findAll({
    attributes: ["name", "email"],
    include: [
      {
        model: Post,
        attributes: ["post_name"],
      },
    ],
    where: { id: 1 },
  });
  // const data = await Post.findAll({ where: { id: 3 } });

  res.json(data);
};

const belongsTo = async (req, res) => {
  const data = await Post.findAll({
    include: { model: Users, as: "author", attributes: ["name", "email"] },
  });

  res.json(data);
};

const hasMany = async (req, res) => {
  const data = await Users.findAll({
    attributes: ["name", "email"],
    include: [
      {
        model: Post,
        as: "postDetails",
        attributes: ["post_name"],
      },
    ],
    // where: { id: 2 },
  });

  res.json(data);
};

const manyToMany = async (req, res) => {
  // const data = await Post.findAll({
  //   include: {
  //     model: Tags,
  //     attributes: ["name"],
  //   },
  // });

  const data = await Tags.findAll({
    include: {
      model: Post,
      attributes: ["post_name"],
    },
  });

  res.json(data);
};

const transaction = async (req, res) => {
  const t = await db.sequelize.transaction();

  try {
    const data1 = await Users.create(
      {
        name: "ironman",
        email: "ironman@gmail.com",
        gender: "male",
      },
      { transaction: t }
    );

    const data2 = await Postss.create(
      {
        post_name: "smart watch",
        user_id: 1,
      },
      { transaction: t }
    );

    //commiting
    await t.commit();
    console.log("commited");
  } catch (err) {
    console.log("error");
    await t.rollback();
  }

  res.send("ok");
};

const managedTransaction = async (req, res) => {
  const result = await sequelize.transaction(async (t) => {
    try {
      const post = await Post.create(
        {
          post_name: "databases",
          user_id: 2,
        },
        { transaction: t }
      );

      const user = await Users.create(
        {
          name: "deadpool",
          email: "deadpoolgmail.com",
          gender: "male",
        },
        { transaction: t }
      );

      console.log("commited");
      return;
    } catch (err) {
      console.log("error");
      return err;
    }
  });

  res.json(result);
};

module.exports = {
  addUser,
  crud,
  queryData,
  oneToOne,
  belongsTo,
  hasMany,
  manyToMany,
  transaction,
  managedTransaction,
};
