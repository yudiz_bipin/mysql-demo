"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Students", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      gender: {
        type: Sequelize.STRING,
      },
      roll: {
        type: Sequelize.INTEGER,
      },
      // createdAt: {
      //   allowNull: true,
      //   type: Sequelize.DATE,
      //   default:
      // },
      // updatedAt: {
      //   allowNull: true,
      //   type: Sequelize.DATE,
      //   default: Date.now(),
      // },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Students");
  },
};
